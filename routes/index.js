var express = require('express');
var router = express.Router();
const superuser = require('../superuser.json');
const userCtrl = require("../controllers/user.controller");
const db = require("../models");
const UserModel = db.user_games;
const jwt = require("jsonwebtoken");
/* GET home page. */

router.get('/login', function (req, res, next) {
  res.render('login');
});
// router.get('/register', function (req, res, next) {
//   res.render('register');
// });
router.get('/logout', function (req, res, next) {
  res
    .clearCookie("token")
    .status(200).redirect('/login');
});

router.post('/user/login', function (req, res, next) {
  const { username, password } = req.body;
  UserModel.findOne({ where: { username : username, password : password} })
    .then(async (data) => {
        console.log('data====>', data.level);
        if (data) {
          const accessToken = jwt.sign({id_user : data.id, username : data.username, level : data.level}, 'suhairoh2022',{
            expiresIn: '2 days'
          });     
          res.cookie("token", accessToken).status(200).redirect('/home');
      } else {
        res.status(404).send({
          message: `User not found`
        });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).send({
        message: "Error login"
      });
    });  
  // const result = userCtrl.login(req, res).then(result=>{
  //   res.redirect('/home');
  // }).catch(error => {
  //   res.send('username atau password salah');
  // });
  // console.log(result, 'result')
  // if (username === superuser.username && password === superuser.password) {
  //   req.session.is_logged_in = true;
  //   return res.redirect('/home');
  // }

  // req.session.is_logged_in = false;
  // return res.send('username atau password salah');
});

module.exports = router;
