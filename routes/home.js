const express = require('express');
const router = express.Router();
const authMiddleware = require('../middleware/authMiddleware');
// const authMiddleware = require('../middleware/authJwt');
const { user_games_biodata, user_games, user_games_history } = require('../models');

// router.use(authMiddleware);

router.get('/', authMiddleware, async function (req, res, next) {
  if (!req.session.is_logged_in) {
    return res.redirect('/login');
  } else {
    return res.redirect('/home');
  }
});
router.get('/home', authMiddleware, async function (req, res, next) {
  const totalActive = await user_games.findAll({ where: { isActive: true } });
  const totalNotActive = await user_games.findAll({ where: { isActive: false } });
  const biodata = await user_games_biodata.findAll();
  const history = await user_games_history.findAll();

  var param = {
    totalActive: totalActive.length,
    totalNotActive: totalNotActive.length,
    totalBiodata: biodata.length,
    totalHistory: history.length,
  };

  res.render('home', { current_menu: 'home', title: 'Home', data: param });
});
router.get('/user', authMiddleware, function (req, res, next) {
  res.render('user/list', { current_menu: 'user', title: 'User' });
});
router.get('/user/create', authMiddleware, function (req, res, next) {
  res.render('user/form', { current_menu: 'user', title: 'User' });
});
router.get('/user/update/:id', authMiddleware, async function (req, res, next) {
  const id = req.params.id;
  const data = await user_games.findOne({ where: { id: id } });
  res.render('user/formedit', { current_menu: 'user', title: 'User', data: data });
});
// BIODATA
router.get('/biodata', authMiddleware, function (req, res, next) {
  res.render('biodata/list', { current_menu: 'biodata', title: 'Biodata' });
});
router.get('/biodata/create', authMiddleware, async function (req, res, next) {
  const username = await user_games.findAll();
  res.render('biodata/form', { current_menu: 'biodata', title: 'Biodata', username: username });
});
router.get('/biodata/update/:id', authMiddleware, async function (req, res, next) {
  const id = req.params.id;
  const data = await user_games_biodata.findOne({ where: { id: id } });
  const username = await user_games.findAll();

  res.render('biodata/formedit', { current_menu: 'biodata', title: 'Biodata', username: username, data: data });
});

// HISTORY
router.get('/history', authMiddleware, function (req, res, next) {
  res.render('history/list', { current_menu: 'history', title: 'History' });
});
router.get('/history/create', authMiddleware, async function (req, res, next) {
  const username = await user_games.findAll();
  res.render('history/form', { current_menu: 'history', title: 'History', username: username });
});
router.get('/history/update/:id', authMiddleware, async function (req, res, next) {
  const id = req.params.id;
  const data = await user_games_history.findOne({ where: { id: id } });
  const username = await user_games.findAll();

  res.render('history/formedit', { current_menu: 'history', title: 'History', username: username, data: data });
});

module.exports = router;
