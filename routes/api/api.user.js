module.exports = app => {
  const userCtrl = require("../../controllers/user.controller");
  const middleWare = require("../../middleware/authMiddleware");
  var bodyParser = require('body-parser');

  var router = require("express").Router();
  // Create a new User
  router.post("/create", userCtrl.create);

  // Retrieve all userCtrl
  router.get("/", userCtrl.findAll);

  // Retrieve all published userCtrl
  router.get("/published", userCtrl.findAllPublished);

  // Retrieve a single User with id
  router.get("/:id", userCtrl.findOne);

  // Update a User with id
  router.put("/:id", userCtrl.update);

  // Delete a User with id
  router.delete("/:id", userCtrl.delete);

  // Delete all userCtrl
  router.delete("/", userCtrl.deleteAll);
  // app.use(middleWare);
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
  extended: true
  }));
  app.use("/api/user", router);
};
