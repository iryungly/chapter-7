module.exports = app => {
  const userCtrl = require("../../controllers/user.controller");
  var bodyParser = require('body-parser');

  var router = require("express").Router();
  router.post("/", userCtrl.login);
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
  extended: true
  }));
  app.use("/api/login", router);
};
