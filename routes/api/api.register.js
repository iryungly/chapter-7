module.exports = app => {
  const userCtrl = require("../../controllers/user.controller");
  var bodyParser = require('body-parser');

  var router = require("express").Router();
  router.post("/", userCtrl.register);
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
  extended: true
  }));
  app.use("/api/register", router);
};
