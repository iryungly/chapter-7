module.exports = app => {
  const biodataCtrl = require("../../controllers/biodata.controller");
  const middleWare = require("../../middleware/authMiddlewareApi");
  var bodyParser = require('body-parser');

  var router = require("express").Router();
  // Create a new User
  router.post("/create", biodataCtrl.create);

  // Retrieve all biodataCtrl
  router.get("/", biodataCtrl.findAll);

  // Retrieve all published biodataCtrl
  router.get("/published", biodataCtrl.findAllPublished);

  // Retrieve a single User with id
  router.get("/:id", biodataCtrl.findOne);

  // Update a User with id
  router.put("/:id", biodataCtrl.update);

  // Delete a User with id
  router.delete("/:id", biodataCtrl.delete);

  // Delete all biodataCtrl
  router.delete("/", biodataCtrl.deleteAll);
  app.use(middleWare);
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
  extended: true
  }));
  app.use("/api/biodata", router);
};
