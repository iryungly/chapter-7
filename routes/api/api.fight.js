module.exports = app => {
  const fightCtrl = require("../../controllers/fight.controller");
  const middleWare = require("../../middleware/authJwt");
  var bodyParser = require('body-parser');
  var router = require("express").Router();
  router.post("/:id", fightCtrl.fight);
  app.use(middleWare);
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
  extended: true
  }));
  app.use("/api/fight", router);
};
