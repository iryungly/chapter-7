module.exports = app => {
  const historyCtrl = require("../../controllers/history.controller");
  const middleWare = require("../../middleware/authMiddleware");
  var bodyParser = require('body-parser');

  var router = require("express").Router();
  // Create a new User
  router.post("/create", historyCtrl.create);

  // Retrieve all historyCtrl
  router.get("/", historyCtrl.findAll);

  // Retrieve all published historyCtrl
  router.get("/published", historyCtrl.findAllPublished);

  // Retrieve a single User with id
  router.get("/:id", historyCtrl.findOne);

  // Update a User with id
  router.put("/:id", historyCtrl.update);

  // Delete a User with id
  router.delete("/:id", historyCtrl.delete);

  // Delete all historyCtrl
  router.delete("/", historyCtrl.deleteAll);
  // app.use(middleWare);
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
  extended: true
  }));
  app.use("/api/history", router);
};
