module.exports = app => {
  const roomCtrl = require("../../controllers/room.controller");
  const middleWare = require("../../middleware/authJwt");
  var bodyParser = require('body-parser');

  var router = require("express").Router();
  // Create a new User
  router.post("/create", roomCtrl.create);

  // Retrieve all roomCtrl
  router.get("/", roomCtrl.findAll);

  // Retrieve all published roomCtrl
  router.get("/all", roomCtrl.showAll);

  // Retrieve a single User with id
  router.get("/:id", roomCtrl.findOne);

  // Update a User with id
  router.put("/:id", roomCtrl.update);

  // Delete a User with id
  router.delete("/:id", roomCtrl.delete);

  // Delete all roomCtrl
  router.delete("/", roomCtrl.deleteAll);
  app.use(middleWare);
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
  extended: true
  }));
  app.use("/api/room", router);
};
