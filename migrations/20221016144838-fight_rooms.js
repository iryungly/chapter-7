'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
     await queryInterface.createTable('fight_rooms', 
     {       
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_room: {
        type: Sequelize.INTEGER
      },
      id_user: {
        type: Sequelize.INTEGER
      },
      input: {
        type: Sequelize.STRING
      },            
      loop: {
        type: Sequelize.INTEGER
      },                         
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },

  async down (queryInterface, Sequelize) {
     await queryInterface.dropTable('fight_rooms');
  }
};
