'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.createTable('user_games_biodatas', {     
      id : {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    id_user: Sequelize.INTEGER,
    fullname: Sequelize.STRING,
    gender: Sequelize.STRING,
    placebirth: Sequelize.STRING,
    datebirth: Sequelize.STRING,  
  } );
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.dropTable('user_games_biodatas');
  }
};
