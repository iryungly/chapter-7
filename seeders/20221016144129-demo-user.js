'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('user_games', [{
      username: 'admin',
      password: 'admin',
      level : 'super_admin',
      isActive : true,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      username: 'player1',
      password: 'player1',
      level : 'user',
      isActive : true,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      username: 'player2',
      password: 'player2',
      level : 'user',
      isActive : true,
      createdAt: new Date(),
      updatedAt: new Date()
    }      
  ]);   
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('user_games', null, {});

  }
};
