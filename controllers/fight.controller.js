const jwt = require("jsonwebtoken");
const db = require("../models");
const roomModel = db.rooms;
const roomFightModel = db.fight_rooms;
const Op = db.Sequelize.Op;

exports.fight = (req, res) => {
  console.log("payload===>", req.payload)
  const id_room = req.params.id;
  const id_user = req.payload.id_user;
  const inputPilihan = req.body.input.toLowerCase();
  const resp = [];

  console.log(inputPilihan, inputPilihan.includes(), 'input')
  if(!inputPilihan){
    return res.status(400).send({
      message:'Silahkan input salah satu dari objek ini P = Paper, R = Rock, S = Scissor 1'
    });
  }
  if(!['p', 'r', 's'].includes(inputPilihan)){
    return res.status(400).send({
      message:'Silahkan input salah satu dari objek ini P = Paper, R = Rock, S = Scissor 2'
    });
  }  
  roomModel.findByPk(id_room)
    .then(data => {
      if (data) {
        roomFightModel.findAll( {where : {
          id_room : id_room,
          id_user : id_user,
        }}).then((cekdata)=> {
          console.log(cekdata.length,'cekdata');
          if(cekdata.length < 3){
            roomFightModel.create({
              id_user : id_user, 
              id_room : id_room,
              input : inputPilihan,
              loop : cekdata.length + 1
            }).then(hasil =>{
              roomFightModel.findAll( {where : {
                id_room : id_room,
                id_user : {[Op.ne]: id_user},
              }}).then((cekdata2)=> {
                  console.log(cekdata2.length, 'cekdata2')
                  if(cekdata.length == cekdata2.length){
                    cekdata.forEach((element, index) => {
                      if(element.input == 'p' && cekdata2[index].input == 'p'){
                        resp.push('Draw');
                      }else
                      if(element.input == 'p' && cekdata2[index].input == 'r'){
                        resp.push('You Win');
                      }else
                      if(element.input == 'p' && cekdata2[index].input == 's'){
                        resp.push('You Lose');
                      }
                      else 
                      if(element.input == 'r' && cekdata2[index].input == 'p'){
                        resp.push('You Lose');
                      }
                      else
                      if(element.input == 'r' && cekdata2[index].input == 'r'){
                        resp.push('Draw');
                      }else
                      if(element.input == 'r' && cekdata2[index].input == 's'){
                        resp.push('You Lose');
                      } 
                      else 
                      if(element.input == 's' && cekdata2[index].input == 'p'){
                        resp.push('You Win');
                      }
                      else
                      if(element.input == 's' && cekdata2[index].input == 'r'){
                        resp.push('You Lose');
                      }else
                      if(element.input == 's' && cekdata2[index].input == 's'){
                        resp.push('Draw');
                      }                     
                    });
                    return res.status(200).send({result : resp});
                  }else{
                    return res.status(200).send({message : 'Anda bermain '+(cekdata.length-1)+', Lawan bermain '+(cekdata2.length-1)+''});

                  }
              });    
            })
          }else{
            roomFightModel.findAll( {where : {
              id_room : id_room,
              id_user : {[Op.ne]: id_user},
            }}).then((cekdata2)=> {
                console.log(cekdata2.length, 'cekdata2')
                if(cekdata.length != cekdata2.length){
                  return res.status(200).send({message : 'Anda bermain '+(cekdata.length-1)+', Lawan bermain '+(cekdata2.length-1)+''});
                }
                else{
                  cekdata.forEach((element, index) => {
                    if(element.input == 'p' && cekdata2[index].input == 'p'){
                      resp.push('Draw');
                    }else
                    if(element.input == 'p' && cekdata2[index].input == 'r'){
                      resp.push('You Win');
                    }else
                    if(element.input == 'p' && cekdata2[index].input == 's'){
                      resp.push('You Lose');
                    }
                    else 
                    if(element.input == 'r' && cekdata2[index].input == 'p'){
                      resp.push('You Lose');
                    }
                    else
                    if(element.input == 'r' && cekdata2[index].input == 'r'){
                      resp.push('Draw');
                    }else
                    if(element.input == 'r' && cekdata2[index].input == 's'){
                      resp.push('You Lose');
                    } 
                    else 
                    if(element.input == 's' && cekdata2[index].input == 'p'){
                      resp.push('You Win');
                    }
                    else
                    if(element.input == 's' && cekdata2[index].input == 'r'){
                      resp.push('You Lose');
                    }else
                    if(element.input == 's' && cekdata2[index].input == 's'){
                      resp.push('Draw');
                    }                     
                  });
                  return res.status(200).send({result : resp});
                }
            });                       

          }
        });
      } else {
        res.status(404).send({
          message: `Room not existed`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving User with id=" + id
      });
    });

};
