const jwt = require("jsonwebtoken");
// const jwtVerify = require("../middleware/authJwt");
const db = require("../models");
const UserModel = db.user_games;
const Op = db.Sequelize.Op;
// Create and Save a new User
exports.create = (req, res) => {
  // Validate request
  console.log(req.body, req.data, req.query, 'body')
  if (!req.body.username ) {
    res.status(400).send({
      message: "Username can not be empty!"
    });
    return;
  }
  if (!req.body.password) {
    res.status(400).send({
      message: "Password can not be empty!"
    });
    return;
  }  

  // Create a User
  const param = {
    username: req.body.username,
    password: req.body.password,
    level: req.body.level,
    isActive: req.body.isActive
  };

  // Save User in the database
  UserModel.create(param)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the User."
      });
    });
};


exports.register = (req, res) => {
  // Validate request
  console.log(req.body, req.data, req.query, 'body')
  if (!req.body.username ) {
    res.status(400).send({
      message: "Username can not be empty!"
    });
    return;
  }
  if (!req.body.password) {
    res.status(400).send({
      message: "Password can not be empty!"
    });
    return;
  }  

  // Create a User
  const param = {
    username: req.body.username,
    password: req.body.password,
    isActive: true,
    level : 'user'
  };

  // Save User in the database
  UserModel.create(param)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the User."
      });
    });
};

// Retrieve all User from the database.
exports.findAll = async (req, res) =>  {
  const title = req.query.title;
  var condition = title ? { title: { [Op.iLike]: `%${title}%` } } : null;

  await UserModel.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving data"
      });
    });
};

// Find a single User with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  UserModel.findByPk(id)
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({
          message: `Cannot find User with id=${id}.`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving User with id=" + id
      });
    });
};

// Update a User by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;
  const param = {
    username: req.body.username,
    password: req.body.password,
    level: req.body.level,
    isActive: req.body.isActive
  };
  UserModel.update(param, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "User was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating User with id=" + id
      });
    });
};

// Delete a User with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  UserModel.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "User was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete User with id=${id}. Maybe User was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete User with id=" + id
      });
    });
};

// Delete all User from the database.
exports.deleteAll = (req, res) => {
  UserModel.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} User were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all data"
      });
    });
};

// find all published User
exports.findAllPublished = (req, res) => {
  UserModel.findAll({ where: { published: true } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving data"
      });
    });
};
check_token = async (token) => {
  const promise =  new Promise( async (resolve, reject) => {
    try {
      const decoded = await jwt.verify(token, 'suhairoh2022');
      resolve(true);
    } catch (err) {
      reject(err);
    }
  });
  return promise;
}
proses_login = (req, res)=> {
  const username = req.body.username;
  const password = req.body.password;  
  UserModel.findOne({ where: { username : username, password : password} })
    .then(async (data) => {
        if (data) {
          const accessToken = jwt.sign({id_user : data.id, username : data.username, level : data.level}, 'suhairoh2022',{
            expiresIn: '2 days'
          });     
          res.cookie("token", accessToken).status(200).json({message : 'Login berhasil', token : accessToken });
      } else {
        res.status(404).send({
          message: `User not found`
        });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).send({
        message: "Error login"
      });
    });
}
// Find a single User with an id
exports.login = async (req, res) => {
  const token = req.body.token || req.query.token || req.headers["x-access-token"];
  // if(token!=null && token != ''){
  //   await check_token(token).then(result=> {
  //     if(result){
  //       res.cookie("token", token).status(200).json({message : 'Verifikasi token berhasil anda sudah login', token : token });
  //     }      
  //   }).catch(error=>{
  //     proses_login(req, res);
  //   });
  // }else{
    proses_login(req, res);
  // }
};