const jwt = require("jsonwebtoken");
const db = require("../models");
const roomModel = db.rooms;
const Op = db.Sequelize.Op;
// Create and Save a new User
exports.create = (req, res) => {
  // Validate request
  console.log(req.body, req.data, req.query, 'body')
  if (!req.body.name ) {
    res.status(400).send({
      message: "Username can not be empty!"
    });
    return;
  } 

  // Create a User
  const param = {
    name: req.body.name,
  };

  // Save User in the database
  roomModel.create(param)
    .then(data => {
      res.json({message : 'Create room success', id : data.id})
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the User."
      });
    });
};

// Retrieve all User from the database.
exports.findAll = async (req, res) =>  {
  const title = req.query.title;
  var condition = title ? { title: { [Op.iLike]: `%${title}%` } } : null;

  await roomModel.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving data"
      });
    });
};

// Find a single User with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  roomModel.findByPk(id)
    .then(data => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({
          message: `Cannot find User with id=${id}.`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving User with id=" + id
      });
    });
};

// Update a User by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;
  const param = {
    username: req.body.username,
    password: req.body.password,
    isActive: req.body.isActive
  };
  roomModel.update(param, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "User was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating User with id=" + id
      });
    });
};

// Delete a User with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  roomModel.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "User was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete User with id=${id}. Maybe User was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete User with id=" + id
      });
    });
};

// Delete all User from the database.
exports.deleteAll = (req, res) => {
  roomModel.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} User were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all data"
      });
    });
};

// find all published User
exports.showAll = (req, res) => {
  roomModel.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving data"
      });
    });
};

// Find a single User with an id
exports.login = (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  roomModel.findOne({ where: { username : username, password : password} })
    .then(async (data) => {
        if (data) {
          const accessToken = jwt.sign({username, password}, 'suhairoh2022',{
            expiresIn: '2 days'
        });     
        res.json({message : 'Login berhasil', token : accessToken });
      } else {
        res.status(404).send({
          message: `User not found`
        });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).send({
        message: "Error login"
      });
    });
};