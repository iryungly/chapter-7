'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user_games_biodatas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      user_games_biodatas.belongsTo(models.user_games, { foreignKey: "id_user" })
    }
  }
  user_games_biodatas.init({
    id : {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    id_user: DataTypes.INTEGER,
    fullname: DataTypes.STRING,
    gender: DataTypes.STRING,
    placebirth: DataTypes.STRING,
    datebirth: DataTypes.STRING,  
  }, {
    sequelize,
    modelName: 'user_games_biodatas',
  });
  return user_games_biodatas;
};