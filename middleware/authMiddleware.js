// const authMiddleware = (req, res, next) => {
// 	if (!req.session.is_logged_in) {
// 		return res.redirect('/login');
// 	}
// 	next();
// };

// module.exports = authMiddleware;

const jwt = require("jsonwebtoken");

const verifyToken = (req, res, next) => {
  const token =
  req.cookies.token || req.body.token || req.query.token || req.headers["x-access-token"];
  console.log('token===>', token);
  if (!token) {
    return res.redirect('/login');
  }
  try {
    const decoded = jwt.verify(token, 'suhairoh2022');
	console.log("decoded===>", decoded.level);
	if(decoded.level != 'super_admin'){
		return res.send({'message' : 'Level anda bukan admin'});
	}
    req.payload = decoded;
  } catch (err) {
    return res.redirect('/login');
  }
  return next();
};

module.exports = verifyToken;