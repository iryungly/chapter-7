const authMiddleware = (req, res, next) => {
	req.session.is_logged_in = true;
	if (!req.session.is_logged_in) {
		res.status(440).send({
			message: "Session anda habis, silahkan logout dan login kembali"
		  });
	}
	next();
};

module.exports = authMiddleware;
